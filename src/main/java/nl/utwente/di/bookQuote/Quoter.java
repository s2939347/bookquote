package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> isbnResults;

    public Quoter() {
        isbnResults = new HashMap<>();
        isbnResults.put("1", 10.0);
        isbnResults.put("2", 45.0);
        isbnResults.put("3", 20.0);
        isbnResults.put("4", 35.0);
        isbnResults.put("5", 50.0);
        isbnResults.put(null, 0.0); //supposed to signify "Others"
    }
    double getBookPrice(String isbn) {
        return isbnResults.get(isbn);
    }
}
